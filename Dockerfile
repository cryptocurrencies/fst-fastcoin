# ---- Base Node ---- #
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libevent-dev libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/fastcoinproject/fastcoin.git /opt/fastcoin && \
	chmod +x /opt/fastcoin/autogen.sh
RUN cd /opt/fastcoin/ && \
    ./autogen.sh && \
    ./configure --without-miniupnpc --disable-tests --enable-debug --with-gui=no
RUN cd /opt/fastcoin/ && \
	chmod +x share/genbuild.sh && \
    chmod +x ./src/leveldb/build_detect_platform && \
    make

# ---- Release ---- #
FROM ubuntu:16.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev libevent-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r fastcoin && useradd -r -m -g fastcoin fastcoin
RUN mkdir /data
RUN chown fastcoin:fastcoin /data
COPY --from=build /opt/fastcoin/src/fastcoind /usr/local/bin/fastcoind
COPY --from=build /opt/fastcoin/src/fastcoin-cli /usr/local/bin/fastcoin-cli
USER fastcoin
VOLUME /data
EXPOSE 5888 5889
CMD ["/usr/local/bin/fastcoind", "-datadir=/data", "-conf=/data/fastcoin.conf", "-server", "-txindex", "-printtoconsole"]